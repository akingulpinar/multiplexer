#include "stdafx.h"

//Beginning of the program
int main() {
	//Defining the public variables which will be used in the program.
	//option variable is for the menu options.
	//base variable is for choosing the base.
	//valBase10 variable is created for binary values of base 10 values.
	//valBase16 variable is created for binary values of base 16 values.
	//binary is used in base 16 for character binaries.
	//controller is for make the program work unless value is 0.
	//bin0 and bin1 is used for select lines, line0,line1,line2,line3 is used for base 2 binaries.
	int option, base, valBase10, valBase16, binary, controller = 1;
	char bin0=0, bin1=0, line0=0, line1=0, line2=0, line3=0, c;
	//Program starts in a loop in order to work unless exit command runs.
	do {
		printf("4 to 1 Multiplexer!\n(1) Compute and display the output\n(2) Quit\nYou choose: ");			//Choosing the option
		scanf("%d", &option);
		if (option != 1 && option != 2) {
			printf("Wrong option. Please try again!\n\n");
			while ((c = getchar()) != '\n' && c != EOF);
			continue;
		}
		else {
			if (option == 1) {
				printf("You have chosen option 1\n");
				do {				//Another loop start in order to be sure of correct values entered.
					while ((c = getchar()) != '\n' && c != EOF);
					int counter = 0; 			//to count the number of digits
					char tempBin0, tempBin1, counterBin; 			//temporary variables for assigning binary values.
					printf("Please enter select lines: ");
					scanf("%c", &tempBin0);
					counterBin = tempBin0;
					while (counterBin != 10)			//the loop which will run until enter key is pressed.It will assign every single char to a variable.
					{
						scanf("%c", &counterBin);
						if (counter == 0) {
							tempBin1 = counterBin;
						}
						counter++;
					}
					if (counter > 2) {			//Giving an error if the lines are wrong
						printf("It is not a 2-digit binary number for the select lines. Please try again!\n");
						continue;
					}
					if ((tempBin0 == 48 || tempBin0 == 49) && (tempBin1 == 48 || tempBin1 == 49)) {				//To check if the numbers are binary.
						bin1 = tempBin0;
						bin0 = tempBin1;
						do {
							printf("Which base will you use to enter data lines (base 16/10/2)?");
							scanf("%d", &base);
							
							if (base != 2 && base != 10 && base != 16) {			//To check the base type.
								printf("Wrong Base. Please try again!\n");
								while ((c = getchar()) != '\n' && c != EOF);
							}
							else {
								char tempLine0 = 48, tempLine1 = 48, tempLine2 = 48, tempLine3 = 48;			//Defining temporary zero chars to be used in getting lines.
								while ((c = getchar()) != '\n' && c != EOF);
								do {			//Getting the lines to convert binary.
									counter = 0;
									controller = 1;
									printf("Please enter the data lines to decode: ");
									scanf("%c", &tempLine0);
									counterBin = tempLine0;
									while (counterBin != 10)			//the loop which will run until enter key is pressed.It will assign every single char to a variable.
									{
										scanf("%c", &counterBin);
										if (counter < 4) {
											if (counter == 0) {
												tempLine1 = counterBin;
											}
											else if (counter == 1) {
												tempLine2 = counterBin;
											}
											else if (counter == 2) {
												tempLine3 = counterBin;
											}
										}
										counter++;
									}
									if (counter < 4) {			//This part will make the empty values 0, such as if the user enter 101 it will be 0101.
										tempLine3 = tempLine2;
										tempLine2 = tempLine1;
										tempLine1 = tempLine0;
										tempLine0 = 48;
									}
									if (counter < 3) {
										tempLine3 = tempLine2;
										tempLine2 = tempLine1;
										tempLine1 = 48;
										tempLine0 = 48;
									}
									if (counter < 2) {
										tempLine3 = tempLine2;
										tempLine2 = 48;
										tempLine1 = 48;
										tempLine0 = 48;
									}
									if (base == 2) {			//Checking the number of digits and giving errors
										if ((tempLine3 != 48 && tempLine3 != 49) || (tempLine2 != 48 && tempLine2 != 49) || (tempLine1 != 48 && tempLine1 != 49) || (tempLine0 != 48 && tempLine0 != 49)) {
											printf("It is not binary number for the data lines. Please try again!\n");
											controller = 0;
											continue;
										}
										if (counter > 4) {
											printf("You cannot enter more than 4 bits. Please try again!\n");
											controller = 0;
											counter = 0;
											continue;
										}
									}
									else if (base == 10) {
										if ((counter > 1 && tempLine2 > 49) || (counter == 2 && tempLine2 == 49 && tempLine3 > 53)) {
											printf("It is not possible to convert %c%c to 4-digit binary number. Please try again!\n", tempLine2, tempLine3);
											controller = 0;
											continue;
										}
									}
									else {
										if (counter > 1 || (tempLine3 < 48 || (tempLine3 > 57 && tempLine3 < 65) || (tempLine3 > 70 && tempLine3 < 97) || tempLine3>102)) {
											printf("It is not possible to convert %c%c to 4-digit binary number. Please try again!\n", tempLine2,tempLine3);
											controller = 0;
											continue;
										}
									}
									
									int remainder = 0, i = 1;
									printf("Output is ");
									switch (base) {			//Converting to binary and printing the output.
									case 2: {			//Assigning temporary values as real line values.										
										line0 = tempLine0;
										line1 = tempLine1;
										line2 = tempLine2;
										line3 = tempLine3;
										if (bin0 == 48 && bin1 == 48) {				//Printing the result.
											printf("%c\n\n", line3);
										}
										else if (bin0 == 48 && bin1 == 49) {
											printf("%c\n\n", line2);
										}
										else if (bin0 == 49 && bin1 == 48) {
											printf("%c\n\n", line1);
										}
										else {
											printf("%c\n\n", line0);
										}
										break;
									}
									case 10: {
										binary = 0;
										int factor = 1;
										if (counter > 0) {
											valBase10 = ((tempLine2 - 48) * 10) + tempLine3 - 48;			//Getting the value in base 10.
										}
										else {
											valBase10 = tempLine2 - 48;
										}
										while (valBase10 != 0) {			//Converting the value in base 10 to 2.
											remainder = valBase10 % 2;
											valBase10 /= 2;
											binary += (remainder*i);
											i *= 10;
										}
										int binary2 = binary;
										counter = 0;
										while (binary2) {			//Getting the digits of binary.
											binary2 = binary2 / 10;
											factor = factor * 10;
											counter++;
										}
										while (factor > 1) {
											factor = factor / 10;
											if (counter == 4) {
												line3 = binary / factor;
											}
											else if (counter == 3) {
												line2 = binary / factor;
											}
											else if (counter == 2) {
												line1 = binary / factor;
											}
											else if (counter == 1) {
												line0 = binary / factor;
											}
											binary = binary % factor;
											counter--;
										}
										if (bin0 == 48 && bin1 == 48) {				//Printing the result.
											printf("%d\n\n", line3);
										}
										else if (bin0 == 48 && bin1 == 49) {
											printf("%d\n\n", line2);
										}
										else if (bin0 == 49 && bin1 == 48) {
											printf("%d\n\n", line1);
										}
										else {
											printf("%d\n\n", line0);
										}
										break;
									}
									case 16: {			//Assigning binary values regarding to the char values.
										binary = 0;
										int factor = 1;
										if (tempLine3 == 'a' || tempLine3 == 'A') {
											binary = 1010;
										}
										else if (tempLine3 == 'b' || tempLine3 == 'B') {
											binary = 1011;
										}
										else if (tempLine3 == 'c' || tempLine3 == 'C') {
											binary = 1100;
										}
										else if (tempLine3 == 'd' || tempLine3 == 'D') {
											binary = 1101;
										}
										else if (tempLine3 == 'e' || tempLine3 == 'E') {
											binary = 1110;
										}
										else if (tempLine3 == 'f' || tempLine3 == 'F') {
											binary = 1111;
										}
										else {
											if (counter > 0) {
												valBase16 = ((tempLine2 - 48) * 10) + tempLine3 - 48;			//Getting the value in base 16.
											}
											else {
												valBase16 = tempLine2 - 48;
											}
											while (valBase16 != 0) {			//Converting the value in base 16 to 2.
												remainder = valBase16 % 2;
												valBase16 /= 2;
												binary += (remainder*i);
												i *= 10;
											}
										}
										int binary2 = binary;
										counter = 0;
										while (binary2) {
											binary2 = binary2 / 10;
											factor = factor * 10;
											counter++;
										}
										while (factor > 1) {			//Getting the digits of binary.
											factor = factor / 10;
											if (counter == 4) {
												line3 = binary / factor;
											}
											else if (counter == 3) {
												line2 = binary / factor;
											}
											else if (counter == 2) {
												line1 = binary / factor;
											}
											else if (counter == 1) {
												line0 = binary / factor;
											}
											binary = binary % factor;
											counter--;
										}
										if (bin0 == 48 && bin1 == 48) {				//Printing the result.
											printf("%d\n\n", line3);
										}
										else if (bin0 == 48 && bin1 == 49) {
											printf("%d\n\n", line2);
										}
										else if (bin0 == 49 && bin1 == 48) {
											printf("%d\n\n", line1);
										}
										else {
											printf("%d\n\n", line0);
										}
										break;
									}
									}
								} while (line0 != tempLine0 && controller == 0);
							}
						} while (base != 2 && base != 10 && base != 16);
					}
					else {
						printf("It is not a 2-digit binary number for the select lines. Please try again!\n");
						continue;
					}
				} while ((bin0 != 48 && bin0 != 49) && (bin1 != 48 && bin1 != 49));
			}
			else {
				printf("You have chosen option 2\nBye!");
				break;
			}
			option = 0;
		}
	} while (option != 1 && option != 2);
	return 0;
}